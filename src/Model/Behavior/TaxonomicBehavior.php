<?php
namespace Taxonomy\Model\Behavior;

use Cake\ORM\Behavior;
use Cake\ORM\Table;
use Cake\ORM\Query;
use Cake\ORM\Entity;
use Cake\Event\Event;
use ArrayObject;
use I18n\Lib\Lang;
use Cake\Collection\Collection;
use Cake\I18n\I18n;

/**
 * Taxonomic behavior
 */
class TaxonomicBehavior extends Behavior
{

  /**
   * Default configuration.
   *
   * @var array
   */
  protected $_defaultConfig = [];

/**
 * Models disponibles para que se asocie el contenido
 * @var array
 */
  private $__availableModels = ['Categories', 'Tags', 'CategoriesShops'];

/**
 * Asociaciones disponibles
 * @var array
 */
  private $__availableAssociations = ['belongsTo', 'belongsToMany'];


/**
 * Hay dos tipo de posibles tipos de taxonomias
 *
 * `relationable` Cuando desde la edición del contenido solo se relaciona con la categorías (tipo categorías)
 * `tagglable` Cuando desde la edición del contenido se puede escribir libremente (tipo tags)
 * 
 * @var array
 */
  private $__availableTypes = ['relationable', 'taggable'];

/**
 * Configuración inicial para cada asociación
 * 
 * `type`: el tipo
 * @var array
 */
  private $__defaults = [
    'associationType' => 'belongsToMany',
    'foreignKey' => 'category_id',
    'type' => 'relationable',
    'plugin' => 'Taxonomy'
  ];

  public function initialize( array $config)
  {
    $this->config ['models'] = [];
    
    foreach( $config as $model => $_config)
    {
      $this->__attach( $model, $_config);
    }
  }

/**
 * En el caso de que el tipo sea `taggable`, guarda las tags y su relación con el model
 * 
 * @param  Event       $event 
 * @param  ArrayObject $data  
 * @return void
 */
  public function beforeMarshal( Event $event, ArrayObject $data, $options)
  {
    foreach( $this->config( 'models') as $model => $config)
    {
      if( $config ['type'] == 'taggable')
      {
        // Indica si tiene o no idioma
        $has = false;

        $property = $this->_table->$model->property();
        $contents = new ArrayObject();

        if( !array_key_exists( '_translations', $data))
        {
          return;
        }

        // Recorre los idiomas
        foreach( $data ['_translations'] as $key => $lang)
        {
          // Sólo si existe el idioma en los datos
          if( array_key_exists( $key, $data ['_translations']) && array_key_exists( $property, $data ['_translations'][$key]))
          {
            $has = true;
            $tags = $data ['_translations'][$key][$property];

            // Guarda los tags en la tabla de los tags
            if( !empty( $tags))
            {
              $this->__saveTags( $tags, $model, $key, $contents);
            }
            
            // Borra la key del idioma
            unset( $data ['_translations'][$key][$property]);
          }
        }

        // Pone los datos en la propiedad si es que hay qué guardar
        if( $has && $contents->count() > 0)
        {
          $data [$property] = (array)$contents; 
        }
        // Solo pondrá el array vacío en el caso de que anteriormente el contenido tuviera tags (para borrarlas)
        elseif( $has && $contents->count() == 0 && isset( $data [$this->_table->primaryKey()]))
        {
          $count = $this->_table->$model->junction()->find()
            ->where([ $this->_table->$model->foreignKey() => $data [$this->_table->primaryKey()]])
            ->count();

          if( $count > 0)
          {
            $data [$property] = []; 
          }
        }
      }
    }
  }

/**
 * Coloca correctamente las tags en su idioma correspondiente, llamando a _rowMapper()
 * 
 * @param  Event  $event 
 * @param  Query  $query 
 * @return void
 */
  public function beforeFind( Event $event, Query $query)
  {
    $properties = [];

    foreach( $this->config( 'models') as $model => $config)
    {
      if( $config ['type'] == 'taggable')
      {
        $properties [] = $this->_table->$model->property();
      }
    }


    if( !empty( $properties))
    {
      $table = $event->subject()->table();
      $query->formatResults(function ($results) use( $table, $properties) {
        return $this->_rowMapper( $results, $properties);
      }, $query::APPEND);
    }
  }

/**
 * Coloca correctamente las tags en su idioma correspondiente, llamando a _rowMapper()  
 * Usa para ello la propiedad _translations, la misma que usa TranslateTrait para los idiomas
 * A su vez, coloca los tags del idiomas actual, en la propiedad correspondiente
 *
 * Hágase notar que el array que resultará en cada idioma, tendrá una única clave `text` para que sea usado por la librería de Javascript en la edición del contenido
 * 
 * @param  array $results    
 * @param  array $properties Las propiedades a gestionar
 * @return void             
 */
  protected function _rowMapper( $results, $properties)
  {
    return $results->map( function( $result) use( $properties){
      foreach( $properties as $property)
      {
        if( is_array( $result))
        {
          if( isset( $result [$property]))
          {
            $collection = new Collection( $result [$property]);

            foreach( Lang::iso3() as $language => $lang)
            {
              if( empty( $result ['_translations'][$language]))
              {
                $result ['_translations'][$language] = [];
              }

              $contents = $collection->match( ['locale' => $language])->toArray();

              $array = [];

              // Recorre todos los contenidos para ponerle la clave `text`
              foreach( $contents as $content)
              {
                $array [] = ['text' => $content ['title']];
              }

              $result ['_translations'][$language][$property] = $array;
            }
          }
        }
        elseif( isset( $result->$property))
        {
          $collection = new Collection( $result->$property);


          // Recorre los idiomas actuales
          foreach( Lang::iso3() as $language => $lang)
          {
            $contents = $collection->match( ['locale' => $language])->toArray();

            // El idioma actual. Coloca los tags en la propiedad.
            if( I18n::locale() == $language)
            {
              $result->set( $property, $contents);
            }
          }
        }
      }

      return $result;
    });
  }

/**
 * Guarda los tags y guarda los resultados en el objecto `$contents`
 * @param  array $tags     
 * @param  string $model    
 * @param  string $locale   
 * @param  ArrayObject $contents 
 * @return void         
 */
  private function __saveTags( array $tags, $model, $locale, ArrayObject $contents)
  {
    $Table = $this->_table->$model;
    $return = [];

    foreach( $tags as $tag)
    {
      $content = $Table->find()
        ->where([
            $Table->alias() .'.'. $Table->displayField() => $tag ['text'],
            $Table->alias() .'.locale' => $locale
        ], [], true)
        ->first();

      if( !$content)
      {
        $content = $Table->newEntity([
          $Table->displayField() => $tag ['text'],
          'locale' => $locale
        ]);

        $Table->save( $content);
      }

      $contents [] = $content->toArray();
    }
  }


  private function __attach( $model, $config)
  {
    list( $plugin, $model) = pluginSplit( $model);

    if( !$plugin)
    {
      $plugin = 'Taxonomy';
    }

    $config ['plugin'] = $plugin;
    $config = array_merge( $this->__defaults, $config);

    $models = $this->config ['models'];
    $models [$model] = $config;
    $this->config( 'models', $models);

    // Exception cuando el model no está entre los disponibles
    if( !in_array( $model, $this->__availableModels))
    {
      throw new \RuntimeException( vsprintf( "TaxonomicBehavior: No available model %s", [$model]));
    }

    // Exception cuando la asociación no está entre las disponibles
    if( !in_array( $config ['associationType'], $this->__availableAssociations))
    {
      throw new \RuntimeException( vsprintf( "TaxonomicBehavior: No available association %s", [$config ['associationType']]));
    }

    // Exception cuando la asociación no está entre las disponibles
    if( !in_array( $config ['type'], $this->__availableTypes))
    {
      throw new \RuntimeException( vsprintf( "TaxonomicBehavior: No available type %s", [$config ['type']]));
    }

    $method = '__'. $config ['associationType'];

    $this->$method( $model, $config);
  }

/**
 * Asocia la tabla con el model mediante la asociación belongsTo
 * 
 * @param  string $model
 * @param  array $config
 * @return void
 */
  private function __belongsTo( $model, $config)
  {
       // Exception si la tabla no tiene la clave foránea
    if( !$this->_table->hasField( $config ['foreignKey']))
    {
      throw new \RuntimeException( vsprintf( "The table `%s` not containing the column `%s`", [$this->_table->table(), $config ['foreignKey']]));
    }
    
    $this->_table->belongsTo( $model, [
      'className' => $config ['plugin'] .'.'. $model,
      'foreignKey' => $config ['foreignKey']
    ]);

    $this->_table->crud->addAssociations( [$model]);
  }

/**
 * Asocia la tabla con el model mediante la asociación belongsToMany
 * 
 * @param  string $model
 * @param  array $config
 * @return void
 */
  private function __belongsToMany( $model, $config)
  {
    $relation_model = $model .'Relations';

    $this->_table->belongsToMany( $model, [
      'className' => $config ['plugin'] .'.'. $model,
      'foreignKey' => 'content_id',
      'targetForeignKey' => 'term_id',
      'joinTable' => 'taxonomy_relationships',
      'through' => $config ['plugin'] .'.'. $relation_model,
    ]);
    
    $this->_table->crud->addAssociations( [$model]);
  }

}
