<?php
namespace Taxonomy\Model\Entity;

use Cake\ORM\Entity;
use Manager\Model\Entity\CrudEntityTrait;

/**
 * Tag Entity.
 */
class Tag extends Entity
{
  use CrudEntityTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
  protected $_accessible = [
      'id' => true,
      'content_type' => true,
      'site_id' => true,
      'slug' => true,
      'title' => true,
      'locale' => true
  ];
}
