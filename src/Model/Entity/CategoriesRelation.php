<?php
namespace Taxonomy\Model\Entity;

use Cake\ORM\Entity;

/**
 * Categoring Entity.
 */
class CategoriesRelation extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'content_id' => true,
        'term_id' => true,
        'model' => true,
    ];
}
