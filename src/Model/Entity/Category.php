<?php
namespace Taxonomy\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use Slug\Model\Entity\SlugTrait;

/**
 * Category Entity.
 */
class Category extends Entity
{

  use CrudEntityTrait;
  use TranslateTrait;
  use SlugTrait;
  /**
   * Fields that can be mass assigned using newEntity() or patchEntity().
   *
   * @var array
   */
  protected $_accessible = [
    '*' => true,
    'content_type' => true,
    'site_id' => true,
    'slug' => true,
    'slugs' => true,
    'title' => true,
  ];
}
