<?php
namespace Taxonomy\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\Validation\Validator;
use Taxonomy\Model\Entity\Category;

/**
 * Category Model
 */
class CategoriesTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'taxonomy_terms');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( Configure::read( 'I18n.behavior'), [
      'fields' => ['title']
    ]);

    $this->addBehavior( 'Manager.Crudable');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Contentable');
    $this->addBehavior( 'Slug.Sluggable');


    // CRUD Config
    $this->crud
      ->addFields([
        'title' => __d( 'admin', 'Nombre'),

      ])->addIndex( 'index', [
        'fields' => [
          'title',
        ],
        'actionButtons' => ['create'],
        'template' => 'Manager/tree',
        'nolevels' => true
      ])->setName( [
        'singular' => __d( 'admin', 'Categoría'),
        'plural' => __d( 'admin', 'Categorías'),
      ])->addView( 'create', [
        'columns' => [
          [
            'title' => 'General',
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                  'slugs'
                ]
              ]
            ]
          ]
        ],
        'actionButtons' => ['index', 'create'],
      ], ['update']);

    $this->addBehavior( 'Seo.Seo');
  }

  

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator) 
  {
    // $validator
    //   ->add( 'id', 'valid', ['rule' => 'numeric'])
    //   ->allowEmpty( 'id', 'create')
    //   ->requirePresence( 'title', 'create')
    //   ->notEmpty( 'title');

    return $validator;
  }
}
