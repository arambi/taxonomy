<?php
namespace Taxonomy\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Taxonomy\Model\Entity\Tag;

/**
 * Tags Model
 */
class TagsTable extends Table
{

  /**
   * Initialize method
   *
   * @param array $config The configuration for the Table.
   * @return void
   */
  public function initialize(array $config)
  {
    $this->table( 'taxonomy_terms');
    $this->displayField( 'title');
    $this->primaryKey( 'id');

    // Behaviors
    $this->addBehavior( 'Timestamp');
    $this->addBehavior( 'Cofree.Saltable');
    $this->addBehavior( 'Cofree.Contentable');
    $this->addBehavior( 'Cofree.Sluggable');

    $this->addBehavior( 'Manager.Crudable');

    // CRUD Config
    $this->crud
      ->addFields([
        'title' => 'Nombre',
      ])->addIndex( 'index', [
        'fields' => [
          'title',
        ],
        'actionButtons' => ['create']
      ])->setName( [
        'singular' => __d( 'admin', 'Etiqueta'),
        'plural' => __d( 'admin', 'Etiquetas'),
      ])->addView( 'create', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                ]
              ]
            ]
          ]
        ],
      ])->addView( 'update', [
        'columns' => [
          [
            'cols' => 8,
            'box' => [
              [
                'elements' => [
                  'title',
                ]
              ]
            ]
          ],
        ],
        'actionButtons' => ['create', 'index']
      ]);
  }

  /**
   * Default validation rules.
   *
   * @param \Cake\Validation\Validator $validator Validator instance.
   * @return \Cake\Validation\Validator
   */
  public function validationDefault(Validator $validator)
  {
    $validator
        ->notEmpty('title');

    return $validator;
  }
}
