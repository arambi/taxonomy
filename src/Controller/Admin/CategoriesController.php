<?php
namespace Taxonomy\Controller\Admin;

use Taxonomy\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Categories Controller
 *
 * @property \Taxonomy\Model\Table\CategoriesTable $Categories
 */
class CategoriesController extends AppController
{
  use CrudControllerTrait;

  public function index()
  {
    $this->CrudTool->addSerialized( [
      'contents' => $this->Categories->tree()
    ]);
  }
}
