<?php
namespace Taxonomy\Controller\Admin;

use Taxonomy\Controller\AppController;
use Manager\Controller\CrudControllerTrait;

/**
 * Tags Controller
 *
 * @property \Taxonomy\Model\Table\TagsTable $Tags
 */
class TagsController extends AppController 
{
  use CrudControllerTrait;

  public function autocomplete()
  {
    $this->CrudTool->serializeAction( false);

    $contents = $this->Table->find()
      ->where([
        $this->Table->alias() .'.'. $this->Table->displayField() .' LIKE' => '%'.$this->request->query ['query'].'%'
      ])
      ->select([
        'text' => $this->Table->alias() .'.'. $this->Table->displayField()
      ])
      ->all();

    foreach( $contents as $content)
    {
      unset( $content->full_title);
      unset( $content->urls); 
    }
    $this->set([
      'data' => $contents,
      '_serialize' => ['data'],
    ]);
  }
}
