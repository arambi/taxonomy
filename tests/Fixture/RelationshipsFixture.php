<?php
namespace Taxonomy\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * RelationshipsFixture
 *
 */
class RelationshipsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'taxonomy_relationships';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'content_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'term_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'model' => ['type' => 'string', 'length' => 36, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'content_id' => ['type' => 'index', 'columns' => ['content_id'], 'length' => []],
            'term_id' => ['type' => 'index', 'columns' => ['term_id'], 'length' => []],
            'model' => ['type' => 'index', 'columns' => ['model'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
      [
        'id' => 1,
        'content_id' => 1,
        'term_id' => 2,
        'model' => 'Tags',
        'created' => '2015-03-03 21:51:12'
      ],
      [
        'id' => 2,
        'content_id' => 1,
        'term_id' => 3,
        'model' => 'Tags',
        'created' => '2015-03-03 21:51:12'
      ],
      [
        'id' => 3,
        'content_id' => 1,
        'term_id' => 4,
        'model' => 'Tags',
        'created' => '2015-03-03 21:51:12'
      ],
      [
        'id' => 4,
        'content_id' => 1,
        'term_id' => 5,
        'model' => 'Tags',
        'created' => '2015-03-03 21:51:12'
      ],
      [
        'id' => 5,
        'content_id' => 1,
        'term_id' => 1,
        'model' => 'Categories',
        'created' => '2015-03-03 21:51:12'
      ],
    ];
}
