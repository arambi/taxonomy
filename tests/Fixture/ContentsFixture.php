<?php
namespace Taxonomy\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContentsFixture
 *
 */
class ContentsFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
  public $fields = [
    'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
    'content_type' => ['type' => 'string', 'length' => 16, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'site_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'parent_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'subtype' => ['type' => 'string', 'length' => 32, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'category_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'slug' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'published' => ['type' => 'boolean', 'length' => null, 'null' => true, 'default' => '0', 'comment' => '', 'precision' => null],
    'published_at' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'title' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'antetitle' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'subtitle' => ['type' => 'string', 'length' => 255, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
    'summary' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'body' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'row' => ['type' => 'integer', 'length' => 3, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'position' => ['type' => 'integer', 'length' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
    'settings' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'photo' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'salt' => ['type' => 'string', 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
    '_indexes' => [
      'content_type' => ['type' => 'index', 'columns' => ['content_type'], 'length' => []],
      'site_id' => ['type' => 'index', 'columns' => ['site_id'], 'length' => []],
      'parent_id' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
      'category_id' => ['type' => 'index', 'columns' => ['category_id'], 'length' => []],
      'slug' => ['type' => 'index', 'columns' => ['slug'], 'length' => []],
      'parent_id_2' => ['type' => 'index', 'columns' => ['parent_id'], 'length' => []],
      'published_at' => ['type' => 'index', 'columns' => ['published_at'], 'length' => []],
      'row' => ['type' => 'index', 'columns' => ['row'], 'length' => []],
      'position' => ['type' => 'index', 'columns' => ['position'], 'length' => []],
    ],
    '_constraints' => [
      'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
    ],
    '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
    ],
  ];

/**
 * Records
 *
 * @var array
 */
  public $records = [
    [
      'id' => 1,
      'content_type' => 'Contents',
      'site_id' => 1,
      'parent_id' => 1,
      'subtype' => '',
      'category_id' => 1,
      'slug' => 'el-titulo',
      'published' => 1,
      'published_at' => '2014-12-03',
      'title' => 'El título',
      'antetitle' => '',
      'subtitle' => '',
      'body' => '',
      'row' => 1,
      'position' => 1,
      'settings' => '',
      'photo' => '',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ],
    [
      'id' => 2,
      'content_type' => 'Contents',
      'site_id' => 1,
      'parent_id' => 1,
      'subtype' => '',
      'category_id' => 1,
      'slug' => 'otro-titulo',
      'published' => 1,
      'published_at' => '2014-12-03',
      'title' => 'Otro título',
      'antetitle' => '',
      'subtitle' => '',
      'body' => '',
      'row' => 1,
      'position' => 1,
      'settings' => '',
      'photo' => '',
      'created' => '2014-12-03 14:42:21',
      'modified' => '2014-12-03 14:42:21'
    ]
  ];

}
