<?php
namespace Taxonomy\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CategoryFixture
 *
 */
class TermsFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'taxonomy_terms';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'content_type' => ['type' => 'string', 'length' => 16, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'site_id' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'parent_id' => ['type' => 'integer', 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'related_id' => ['type' => 'integer', 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'position' => ['type' => 'integer', 'length' => 4, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'slug' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'title' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'locale' => ['type' => 'string', 'length' => 8, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'model' => ['type' => 'string', 'length' => 36, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'content_type' => ['type' => 'index', 'columns' => ['content_type'], 'length' => []],
            'site_id' => ['type' => 'index', 'columns' => ['site_id'], 'length' => []],
            'slug' => ['type' => 'index', 'columns' => ['slug'], 'length' => []],
            'locale' => ['type' => 'index', 'columns' => ['locale'], 'length' => []],
            'model' => ['type' => 'index', 'columns' => ['model'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
      [
        'id' => 1,
        'content_type' => 'Categories',
        'site_id' => null,
        'slug' => 'politica',
        'title' => 'Politica',
        'created' => '2015-03-03 21:29:37',
        'modified' => '2015-03-03 21:29:37'
      ],
      [
        'id' => 2,
        'content_type' => 'Tags',
        'site_id' => null,
        'slug' => 'etiqueta-1',
        'title' => 'Etiqueta 1',
        'locale' => 'spa',
        'created' => '2015-03-03 21:29:45',
        'modified' => '2015-03-03 21:29:45'
      ],
      [
        'id' => 3,
        'content_type' => 'Tags',
        'site_id' => null,
        'slug' => 'etiqueta-2',
        'title' => 'Etiqueta 2',
        'locale' => 'spa',
        'created' => '2015-03-03 21:29:45',
        'modified' => '2015-03-03 21:29:45'
      ],
      [
        'id' => 4,
        'content_type' => 'Tags',
        'site_id' => null,
        'slug' => 'tag-1',
        'title' => 'Tag 1',
        'locale' => 'eng',
        'created' => '2015-03-03 21:29:45',
        'modified' => '2015-03-03 21:29:45'
      ],
      [
        'id' => 5,
        'content_type' => 'Tags',
        'site_id' => null,
        'slug' => 'tag-2',
        'title' => 'Tag 2',
        'locale' => 'eng',
        'created' => '2015-03-03 21:29:45',
        'modified' => '2015-03-03 21:29:45'
      ],
    ];
}
