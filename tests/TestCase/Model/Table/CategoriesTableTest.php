<?php
namespace Taxonomy\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Taxonomy\Model\Table\CategoriesTable;
use Cake\I18n\I18n;
use I18n\Lib\Lang;

/**
 * Taxonomy\Model\Table\CategoryTable Test Case
 */
class CategoriesTableTest extends TestCase
{

  /**
   * Fixtures
   *
   * @var array
   */
  public $fixtures = [
      'Categories' => 'plugin.taxonomy.terms',
      'plugin.i18n.languages',
      'plugin.taxonomy.translates',
  ];

  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    parent::setUp();
    $config = TableRegistry::exists( 'Category') ? [] : ['className' => 'Taxonomy\Model\Table\CategoriesTable'];
    $this->Categories = TableRegistry::get( 'Categories', $config);
    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    unset($this->Categories);
    parent::tearDown();
  }

  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }


  public function testSave()
  {
    $this->setLanguages();
    I18n::locale( 'spa');

    $data = [
      'spa' => [
        'title' => 'Economía'
      ],
      'eng' => [
        'title' => 'Economy'
      ]
    ];

    $category = $this->Categories->getNewEntity( $data);
    $this->Categories->saveContent( $category);
    $new = $this->Categories->get( $category->id);
    $this->assertEquals( $category->id, $new->id);
  }

}
