<?php
namespace Taxonomy\Test\TestCase\Model\Behavior;

use Cake\TestSuite\TestCase;
use Taxonomy\Model\Behavior\TaxonomicBehavior;
use Cake\Datasource\ConnectionManager;
use Cake\ORM\Query;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Core\Plugin;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Manager\Model\Entity\CrudEntityTrait;
use I18n\Lib\Lang;
use Cake\I18n\I18n;

/**
 * ContentsTable para que pueda vincular el Behavior
 */
class ContentsTable extends Table 
{

  public function initialize(array $options) 
  {
    $this->entityClass( 'Taxonomy\Test\TestCase\Model\Behavior\Content');
  }
}

class Content extends Entity 
{
  use CrudEntityTrait;
  use TranslateTrait;

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
  protected $_accessible = [
    'title' => true,
    'categories' => true,
    'tags' => true
  ];
} 

/**
 * Taxonomy\Model\Behavior\TaxonomicBehavior Test Case
 */
class TaxonomicBehaviorTest extends TestCase
{
  public $fixtures = [
    'plugin.taxonomy.contents',
    'plugin.taxonomy.relationships',
    'plugin.taxonomy.terms',
    'plugin.i18n.languages',
    'plugin.taxonomy.translates',
  ];


  /**
   * setUp method
   *
   * @return void
   */
  public function setUp()
  {
    // Quita el plugin section si está leído
    if( Plugin::loaded( 'Section'))
    {
      Plugin::unload( 'Section');
    }

    // Quita el plugin Website si está leído
    if( Plugin::loaded( 'Website'))
    {
      Plugin::unload( 'Website');
    }

    parent::setUp();

    $this->connection = ConnectionManager::get('test');
    $this->Contents = new ContentsTable([
      'alias' => 'Contents',
      'table' => 'contents',
      'connection' => $this->connection
    ]);

    $this->Languages = TableRegistry::get( 'Langs', ['table' => 'languages']);
  }

  /**
   * tearDown method
   *
   * @return void
   */
  public function tearDown()
  {
    TableRegistry::clear();
    unset( $this->Contents);
    unset( $this->connection);
    parent::tearDown();
  }


  public function setLanguages()
  {
    Lang::set( $this->Languages->find()->all());
  }


/**
 * Comprueba la inicialización y configuración del behavior
 */
  public function testInitialize()
  {
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Categories' => [
        'associationType' => 'belongsTo',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany'
      ]
    ]);

    // Comrprueba que tiene el behavior
    $this->assertTrue( $this->Contents->hasBehavior( 'Taxonomic'));

    // Comprueba que el behavior a asociado correctamente las tablas
    $associations = $this->Contents->associations();
    $this->assertTrue( $associations->has( 'Categories'));
    $this->assertTrue( $associations->has( 'Tags'));

    // Comprueba que el behavior ha asociado correctamente las tablas y además en su correspondiente tipo de asociación
    $belongs_tos = $associations->type( 'BelongsTo');
    $this->assertEquals( 'Categories', $belongs_tos [0]->name());

    $belongs_to_manies = $associations->type( 'BelongsToMany');
    $this->assertEquals( 'Tags', $belongs_to_manies [0]->name());
  }

  public function testInitialize2()
  {
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Categories' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany'
      ]
    ]);

    // Comrprueba que tiene el behavior
    $this->assertTrue( $this->Contents->hasBehavior( 'Taxonomic'));

    // Comprueba que el behavior a asociado correctamente las tablas
    $associations = $this->Contents->associations();
    $this->assertTrue( $associations->has( 'Categories'));
    $this->assertTrue( $associations->has( 'Tags'));

    // Comprueba que el behavior ha asociado correctamente las tablas y además en su correspondiente tipo de asociación
    $belongs_to_manies = $associations->type( 'BelongsToMany');
    $this->assertEquals( 'Categories', $belongs_to_manies [0]->name());
    $this->assertEquals( 'Tags', $belongs_to_manies [1]->name());
  }


  public function testInitialize3()
  {
    $this->Contents->addBehavior( 'Manager.Crudable');

    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Shop.CategoriesShops' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany'
      ]
    ]);

    // Comrprueba que tiene el behavior
    $this->assertTrue( $this->Contents->hasBehavior( 'Taxonomic'));

    // Comprueba que el behavior a asociado correctamente las tablas
    $associations = $this->Contents->associations();
    $this->assertTrue( $associations->has( 'CategoriesShops'));
    $this->assertTrue( $associations->has( 'Tags'));

    // Comprueba que el behavior ha asociado correctamente las tablas y además en su correspondiente tipo de asociación
    $belongs_to_manies = $associations->type( 'BelongsToMany');
    $this->assertEquals( 'CategoriesShops', $belongs_to_manies [0]->name());
    $this->assertEquals( 'Tags', $belongs_to_manies [1]->name());
  }


  public function testSaveNewTags()
  {
    $this->setLanguages();
    I18n::locale( 'spa');

    $this->Contents->addBehavior( 'Timestamp');
    $this->Contents->addBehavior( 'Manager.Crudable');
    
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);
    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);

    $data = [ 
      'spa' => [
        'title' => 'Un título',
        'tags' => [
          [
            'text' => 'Un tag'
          ],
          [
            'text' => 'Otro tag'
          ]
        ]
      ],
      'eng' => [
        'title' => 'One title',
        'tags' => [
          [
            'text' => 'One tag'
          ],
          [
            'text' => 'Another tag'
          ]
        ]
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    $this->assertEquals( 4, count( $content->tags));
    $saved = $this->Contents->saveContent( $content);

    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();

    $this->assertEquals( 2, count( $content->tags));
  }

/**
 * Va a modificar los tags que ya están en la base de datos
 * Se supone que en la base de datos el contenido está asociado a dos tags en cada idioma
 * 
 * 1. Busca el contenido (el primero de la base de datos)
 * 2. Borra un tag de inglés
 * 3. Busca el contenido y comprueba que se ha borrado el tag
 */
  public function testSaveTags()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);

    // Busca el contenido, tanto la entidad como el array
    $data = $this->Contents->find( 'array')->first();
    $content = $this->Contents->find( 'content')->first();

    // Borra el tag en inglés
    unset( $data ['eng']['tags'][0]);

    // Guarda del contenido
    $content = $this->Contents->patchContent( $content, $data);
    $this->Contents->saveContent( $content);

    // Busca el contenido con el inglés como idioma
    I18n::locale( 'eng');
    $content = $this->Contents->find( 'content')->first();
    $this->assertEquals( 1, count( $content->tags));

    // Busca el contenido con el español como idioma
    I18n::locale( 'spa');
    $content = $this->Contents->find( 'content')->first();
    $this->assertEquals( 2, count( $content->tags));

    $data = $this->Contents->find( 'array')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();
  }

/**
 * Añade tags a un contenido nuevo
 * 
 * 1. Crea el contenido
 * 2. Borra un tag de inglés
 * 3. Busca el contenido y comprueba que se ha borrado el tag
 */
  public function testAppendTags()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);

    // Crea el contenido
    $data = [
      'title' => 'Un título'
    ];

    $content = $this->Contents->getNewEntity( $data);
    
    $this->Contents->saveContent( $content);

    // Busca el array
    $data = $this->Contents->find( 'array')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();

    // Busca el contenido
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();

    // Añade los tags en cada idioma
    $data ['spa'] = [
      'tags' => [
        [
          'text' => 'Un tag'
        ],
        [
          'text' => 'Otro tag'
        ]
      ]
    ];

    $data ['eng'] = [
      'tags' => [
        [
          'text' => 'One tag'
        ],
        [
          'text' => 'Another tag'
        ]
      ]
    ];

    // Guarda el contenido
    $content = $this->Contents->patchContent( $content, $data);
    $this->Contents->saveContent( $content);

    // Comprueba en español
    I18n::locale( 'spa');
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();
    
    $this->assertEquals( 2, count( $content->tags));

    // Comprueba en inglés
    I18n::locale( 'eng');
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();
    $this->assertEquals( 2, count( $content->tags));
  }



  /**
 * Añade tags a un contenido nuevo
 * 
 * 1. Crea el contenido
 * 2. Borra un tag de inglés
 * 3. Busca el contenido y comprueba que se ha borrado el tag
 */
  public function testAppendTagsWithTranslations()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    $this->Contents->addBehavior( 'Timestamp');
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ]
    ]);

    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);

    // Crea el contenido
    $data = [
      'spa' => [
        'title' => 'Un título'
      ],
      'eng' => [
        'title' => 'A title'
      ]
    ];

    $content = $this->Contents->getNewEntity( $data);
    
    $this->Contents->saveContent( $content);

    // Busca el array
    $data = $this->Contents->find( 'array')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();

    // Busca el contenido
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();

    // Añade los tags en cada idioma
    $data ['spa']['tags'] = [
      [
        'text' => 'Un tag'
      ],
      [
        'text' => 'Otro tag'
      ]
    ];

    $data ['eng']['tags'] = [
      [
        'text' => 'One tag'
      ],
      [
        'text' => 'Another tag'
      ]
    ];

    // Guarda el contenido
    $content = $this->Contents->patchContent( $content, $data);

    $this->Contents->saveContent( $content);

    // Comprueba en español
    I18n::locale( 'spa');
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();
    
    $this->assertEquals( 2, count( $content->tags));

    // Comprueba en inglés
    I18n::locale( 'eng');
    $content = $this->Contents->find( 'content')
      ->where([
        'Contents.id' => $content->id
      ])
      ->first();
    $this->assertEquals( 2, count( $content->tags));
  }

  public function testReadTwoAssociations()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    $this->Contents->addBehavior( 'Timestamp');
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Categories' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ],
    ]);

    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);

    $content = $this->Contents->find( 'content')
      ->where( ['Contents.id' => 1])
      ->first();

    $this->assertEquals( 1, count( $content->categories));
    $this->assertEquals( 2, count( $content->tags));
  }


  public function testSaveTwoAssociations()
  {
    $this->setLanguages();
    I18n::locale( 'spa');
    $this->Contents->addBehavior( 'Timestamp');
    $this->Contents->addBehavior( 'Manager.Crudable');
    $this->Contents->addBehavior( 'Taxonomy.Taxonomic', [
      'Categories' => [
        'associationType' => 'belongsToMany',
      ],
      'Tags' => [
        'associationType' => 'belongsToMany',
        'type' => 'taggable'
      ],
    ]);

    $this->Contents->addBehavior( 'I18n.I18nable', [
      'fields' => ['title']
    ]);

    $content = $this->Contents->find( 'content')
      ->where( ['Contents.id' => 2])
      ->first();

    $data = $this->Contents->find( 'array')
      ->where( ['Contents.id' => 2])
      ->first();

    $data ['spa']['tags'] = [
      ['text' => 'Un tag'],
      ['text' => 'Otro tag']
    ]; 

    $data ['eng']['tags'] = [
      [ 'text' => 'A tag'],
      ['text' => 'Another tag']
    ]; 

    $category = $this->Contents->Categories->find()->first()->toArray();
    $data ['categories'][] = $category;

    $this->Contents->patchContent( $content, $data);
    
    $this->Contents->saveContent( $content);

    $content = $this->Contents->find( 'content')
      ->where( ['Contents.id' => 2])
      ->first();

    $this->assertEquals( 1, count( $content->categories));
    $this->assertEquals( 2, count( $content->tags));
  }
}
