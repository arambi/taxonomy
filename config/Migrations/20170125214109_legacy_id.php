<?php

use Phinx\Migration\AbstractMigration;

class LegacyId extends AbstractMigration
{

  public function change()
  {
    $terms = $this->table( 'taxonomy_terms');
   
    if( !$terms->hasColumn( 'legacy_id'))
    {
      $terms
        ->addColumn( 'legacy_id', 'integer', ['null' => true, 'default' => null])
        ->save();
    }
            
  }
}
