<?php

use Phinx\Migration\AbstractMigration;

class Initial extends AbstractMigration
{
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   *
   * Uncomment this method if you would like to use it.
   * 
  public function change()
  {
  }
  */
  
  /**
   * Migrate Up.
   */
  public function up()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms
          // El tipo de contenido
          ->addColumn( 'content_type', 'string', ['limit' => 16])
          ->addColumn( 'parent_id', 'integer', ['null' => false, 'default' => 0, 'limit' => 8])
          ->addColumn( 'related_id', 'integer', ['null' => false, 'default' => 0, 'limit' => 8])
          ->addColumn( 'position', 'integer', ['null' => true, 'default' => null, 'limit' => 8])
          ->addColumn( 'site_id', 'integer', ['null' => true, 'default' => null, 'limit' => 4])
          ->addColumn( 'slug', 'string', ['null' => false, 'limit' => 255])
          ->addColumn( 'photo', 'text', ['null' => true, 'default' => null])
          ->addColumn( 'title', 'string', ['null' => false, 'limit' => 255])
          ->addColumn( 'locale', 'string', ['null' => true, 'default' => null, 'limit' => 8])
          ->addColumn( 'model', 'string', ['null' => false, 'limit' => 36])
          ->addColumn( 'settings', 'text', ['null' => true, 'default' => null])
          
          // Para cuestiones de seguridad
          ->addColumn( 'salt', 'string', ['default' => NULL, 'null' => true])

          ->addColumn( 'created', 'datetime', ['default' => null])
          ->addColumn( 'modified', 'datetime', ['default' => null])
          ->addIndex( ['content_type'])
          ->addIndex( ['site_id'])
          ->addIndex( ['slug'])
          ->addIndex( ['locale'])
          ->addIndex( ['model'])
          ->addIndex( ['parent_id'])
          ->addIndex( ['related_id'])
          ->addIndex( ['position'])
          ->save();


    // Tabla para las relaciones entre contents y categories
    $relationships = $this->table( 'taxonomy_relationships');
    $relationships
          ->addColumn( 'content_id', 'integer', ['null' => false])
          ->addColumn( 'term_id', 'integer', ['null' => false])
          ->addColumn( 'model', 'string', ['null' => true, 'default' => null, 'limit' => 36])
          ->addColumn( 'created', 'datetime', ['default' => null])
          ->addIndex( ['content_id'])
          ->addIndex( ['term_id'])
          ->addIndex( ['model'])
          ->save();
  }

  /**
   * Migrate Down.
   */
  public function down()
  {
    $this->dropTable( 'taxonomy_terms');
    $this->dropTable( 'taxonomy_relationships');
  }
}