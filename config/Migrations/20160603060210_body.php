<?php

use Phinx\Migration\AbstractMigration;

class Body extends AbstractMigration
{
  /**
   * Change Method.
   *
   * Write your reversible migrations using this method.
   *
   * More information on writing migrations is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
   */
  public function change()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms
        ->addColumn( 'body', 'text', ['default' => null, 'null' => true])
        ->update();
  }
}
