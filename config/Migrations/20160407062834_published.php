<?php

use Phinx\Migration\AbstractMigration;

class Published extends AbstractMigration
{
  public function up()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms
        ->addColumn( 'published', 'boolean', ['default' => 1, 'null' => false])
        ->update();
  }

  public function down()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms->removeColumn( 'published');
  }
}
