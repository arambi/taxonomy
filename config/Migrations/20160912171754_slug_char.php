<?php

use Phinx\Migration\AbstractMigration;

class SlugChar extends AbstractMigration
{

  public function change()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms
          // El tipo de contenido
          ->changeColumn( 'content_type', 'string', ['limit' => 32])
          ->save();
  }
}
