<?php

use Phinx\Migration\AbstractMigration;

class ApplyNull extends AbstractMigration
{

  public function change()
  {
    $terms = $this->table( 'taxonomy_terms');
    $terms
          ->changeColumn( 'title', 'string', ['null' => true, 'default' => null, 'limit' => 255])
          ->save();
  }
}
