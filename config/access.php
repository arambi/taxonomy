<?php

$config ['Access'] = [
  'taxonomy' => [
    'name' => __d( 'admin', 'Categorías'),
    'options' => [
      'update' => [
        'name' => __d( 'admin', 'Editar'),
        'nodes' => [
        
          // Categories
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'categories',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'categories',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'categories',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'categories',
            'action' => 'delete',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'categories',
            'action' => 'order',
          ],


          // Tags
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'tags',
            'action' => 'index',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'tags',
            'action' => 'create',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'tags',
            'action' => 'update',
          ],
          [
            'prefix' => 'admin',
            'plugin' => 'Taxonomy',
            'controller' => 'tags',
            'action' => 'delete',
          ],
        ],
      ]
    ]
  ]
];