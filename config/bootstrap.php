<?php 
use Manager\Navigation\NavigationCollection;
use User\Auth\Access;

// CONFIGURACIÓN DE LOS MENUS DE ADMINISTRACIÓN
NavigationCollection::add( [
  'name' => 'Categorización',
  'icon' => 'fa fa-tags',
  'url' => false,
  'key' => 'taxonomy'
]);

NavigationCollection::add( [
  'parent' => 'taxonomy',
  'name' => 'Categorías',
  'parentName' => 'Categorización',
  'plugin' => 'Taxonomy',
  'controller' => 'Categories',
  'action' => 'index',
  'icon' => 'fa fa-tags',
]);

NavigationCollection::add( [
  'parent' => 'taxonomy',
  'name' => 'Etiquetas',
  'parentName' => 'Categorización',
  'plugin' => 'Taxonomy',
  'controller' => 'Tags',
  'action' => 'index',
  'icon' => 'fa fa-tags',
]);


Access::add( 'taxonomy', [
  'name' => __d( 'admin', 'Categorías'),
  'options' => [
    'update' => [
      'name' => __d( 'admin', 'Editar'),
      'nodes' => [
      
        // Categories
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'categories',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'categories',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'categories',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'categories',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'categories',
          'action' => 'order',
        ],


        // Tags
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'tags',
          'action' => 'index',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'tags',
          'action' => 'create',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'tags',
          'action' => 'update',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'tags',
          'action' => 'delete',
        ],
        [
          'prefix' => 'admin',
          'plugin' => 'Taxonomy',
          'controller' => 'tags',
          'action' => 'autocomplete',
        ],
      ],
    ]
  ]
]);